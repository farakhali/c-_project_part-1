cmake_minimum_required(VERSION 3.16.3)
project(main)

option(DEBUG "choose between debug or release" OFF)

if(DEBUG)
    set(CMAKE_BUILD_TYPE=Debug)
    message(STATUS "Build in Debug mode")
else()
    set(CMAKE_BUILD_TYPE=Release)
    message(STATUS "Build in Release mode")
endif()


add_subdirectory(app)
add_subdirectory(src)
