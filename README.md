# CPP_project_Part-1

This repository include c++ tasks in which we mainly have to learn how to structure C++ programm in diifferent directories and someother practice task like `gitignore`, `README.md` and `CMakeLlists.txt` files.

## How to Run task

This tatask include a student CGPA calculator on the basis of subject marks. The directory structure of the app is...

```
project (main directory)
│   README.md
│   CMakeLists.txt
|   .gitignore
└───include
│   └───student.h
└───src
│   └───student.cpp
|   └───CMakeLists.txt
└───app
|   └───main.cpp
|   └───CMakeLists.txt

```

To build the app Run following commands

- `git clone <url(HTTPS)>`
- `cd c-_project_part-1`
- `mkdir build`
- `cd build`
- `cmake -DDEBUG=(ON/OFF) ..`
- `make`
- `cd app`

The final executable name is `./main`.
