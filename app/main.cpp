#include <string>
#include <iostream>
#include "student.h"

int main()
{
    student student_1("Student-1", 10);
    student student_2("Student-2", 11);
    student student_3("Student-3", 12);
    student student_4("Student-4", 13);
    student student_5("Student-5", 14);

    student_1.set_subj_mark("eng", 11);
    student_1.set_subj_mark("urdu", 12);
    student_1.set_cgpa();
    cout << "Student -1 Record \nRoll No.: " << student_1.get_roll_no()
         << "\tAge: " << student_1.get_age()
         << "\t\tCGPA: " << student_1.get_cgpa()
         << "\n\t\t Subjects\n"
         << student_1.print_all_mark();
    cout << student_1.get_subj_mark("eng") << endl;

    // Student-2
    student_2.set_subj_mark("eng", 98);
    student_2.set_subj_mark("urdu", 87);
    student_2.set_subj_mark("phy", 78);
    student_2.set_cgpa();
    cout << "Student -1 Record \nRoll No.: " << student_2.get_roll_no()
         << "\tAge: " << student_2.get_age()
         << "\t\tCGPA: " << student_2.get_cgpa()
         << "\n\t\t Subjects\n"
         << student_2.print_all_mark();
    cout << student_2.get_subj_mark("eng") << endl;

    cout << student_1.get_subj_mark("urdu") << endl;

    student_1.set_subj_mark("math", 13);
    student_1.set_subj_mark("urdu", 14);

    cout << student_2.get_subj_mark("eng") << endl;

    cout << student_1.get_subj_mark("phy") << endl;

    cout << student_2.print_all_mark() << "\n";

    cout << student_1.print_all_mark() << "\n";
}