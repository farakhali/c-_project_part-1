#include <iostream>
#include <string>
#include <map>

using namespace std;

class student
{
private:
  struct recode
  {
    string roll_no;
    int age;
    float cgpa;
  } rcd;
  map<string, int> result;

public:
  student();
  student(std::string rooll_no, int age);
  ~student();

  int get_age();
  void set_age(int st_age);

  float get_cgpa();
  void set_cgpa();

  string get_roll_no();
  void set_roll_no(string rollno);

  void set_subj_mark(string subj, int mark);
  int get_subj_mark(string get_mark);

  int print_all_mark();
};