#include "student.h"
float sum = 0;
int count = 0;

student::student()
{
    std::cout << "calling constructor function\n";
    rcd.age = 0;
    rcd.cgpa = 0;
    rcd.roll_no = "";
}
student::student(std::string roll_no, int age)
{
    cout << "Student Added to Class\n";
    set_age(age);
    set_roll_no(roll_no);
    set_cgpa();
}
student::~student()
{
    std::cout << "calling destructor function \n";
    // result.~map();
}
int student::get_age()
{
    return rcd.age;
}
void student::set_age(int st_age)
{
    rcd.age = st_age;
}
float student::get_cgpa()
{
    return rcd.cgpa;
}
void student::set_cgpa()
{
    rcd.cgpa = 0;
    sum = 0;
    count = 0;
    for (map<string, int>::iterator itr = result.begin(); itr != result.end(); ++itr)
    {

        sum = sum + itr->second;
        count++;
    }
    rcd.cgpa = 4 * (sum / (count * 100));
}
string student::get_roll_no()
{
    return rcd.roll_no;
}
void student::set_roll_no(string rollno)
{
    rcd.roll_no = rollno;
}
void student::set_subj_mark(string subj, int mark)
{
    if (mark < 0 || mark > 100)
    {
        std::cout << subj << " not set due to error: \n\t marks must be within 0 and 100.\n";
        return;
    }
    if (result.find(subj) != result.end())
    {
        cout << subj << " Subject Mark Updated: \n";
        result[subj] = mark;
    }
    else
    {
        cout << "New Subject added\n";
        result.insert(pair<string, int>(subj, mark));
    }
}
int student::get_subj_mark(string get_mark)
{
    if (result.find(get_mark) != result.end())
    {
        cout << "\n"
             << get_mark << " Subject Marks : ";
        return result[get_mark];
    }
    else
    {
        cout << "\nsorry.. Subject Not Found\n";
        return -1;
    }
}
int student::print_all_mark()
{
    int flag = 0;
    for (map<string, int>::iterator itr = result.begin(); itr != result.end(); ++itr)
    {
        flag = 1;
        cout << itr->first << " : " << itr->second << "\t";
    }

    return 0;
}
